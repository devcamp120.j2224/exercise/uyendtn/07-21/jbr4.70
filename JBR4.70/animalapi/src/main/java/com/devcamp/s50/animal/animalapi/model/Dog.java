package com.devcamp.s50.animal.animalapi.model;

public class Dog extends Mammal{

    public Dog(String name) {
        super(name);
        
    }

    @Override
    public String toString() {
        return "Dog [" + super.toString() + "]";
    }
    
    public void greets() {
        System.out.println("woof");
    }
    public void greets(Dog anotherDog) {
        System.out.println("woofwoof");
    }
}
