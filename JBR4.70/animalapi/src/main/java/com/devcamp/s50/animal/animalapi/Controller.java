package com.devcamp.s50.animal.animalapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.animal.animalapi.model.Cat;
import com.devcamp.s50.animal.animalapi.model.Dog;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("cats")
    public ArrayList<Cat> getCatList() {
        //Tạo 3 đối tượng cat
        Cat cat1 = new Cat("Mimi");
        Cat cat2 = new Cat("Lyly");
        Cat cat3 = new Cat("Kat");
        
        ArrayList<Cat> catList = new ArrayList<Cat>();
        catList.add(cat1);
        catList.add(cat2);
        catList.add(cat3);
        return catList;
    }
    
    @CrossOrigin
    @GetMapping("dogs")
    public ArrayList<Dog> getDogList() {
        //Tạo 3 đối tượng dog
        Dog dog1 = new Dog("Mimi");
        Dog dog2 = new Dog("Lyly");
        Dog dog3 = new Dog("Kat");
        
        ArrayList<Dog> dogList = new ArrayList<Dog>();
        dogList.add(dog1);
        dogList.add(dog2);
        dogList.add(dog3);
        return dogList;
    }
}
